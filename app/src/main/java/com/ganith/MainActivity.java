package com.ganith;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.ganith.fragments.OtpSendFragment;
import com.ganith.util.Navigator;
import com.ganith.util.UIStaticData;

public class MainActivity extends BaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UIStaticData.setFragmentActivity(this);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        //setSupportActionBar(toolbar);
        Navigator.replaceFragment(OtpSendFragment.newInstance());
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }
}