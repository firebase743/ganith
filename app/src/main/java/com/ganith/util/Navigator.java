package com.ganith.util;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ganith.R;


public class Navigator {

    private static final String TAG = "Navigator";

    public static void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = UIStaticData.getFragmentActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    public static int backToPreviousFragment(AppCompatActivity activity) {
        FragmentManager fragManager = activity.getSupportFragmentManager();
        int backStackEntryCount = activity.getSupportFragmentManager().getBackStackEntryCount();
        Log.d(TAG, "backToPreviousFragment: backStackEntryCount :: " + backStackEntryCount);
        try {
            if (backStackEntryCount > 0) {
                fragManager.popBackStackImmediate();
                backStackEntryCount--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return backStackEntryCount;
    }

    public static Fragment getVisibleFragment(AppCompatActivity activity) {
        FragmentManager fragManager = activity.getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentById(R.id.frame_container);
        Log.d(TAG, "getVisibleFragment: fragment :: " + fragment);
        return fragment;
    }
}
