package com.ganith.util;

import androidx.fragment.app.FragmentActivity;

public class UIStaticData {

    private static final String TAG = "UIStaticData";

    public static FragmentActivity fragmentActivity;

    public static FragmentActivity getFragmentActivity() {
        return fragmentActivity;
    }

    public static void setFragmentActivity(FragmentActivity fragmentActivity) {
        UIStaticData.fragmentActivity = fragmentActivity;
    }
}
