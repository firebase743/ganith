package com.ganith.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.ganith.R;
import com.ganith.bean.TracksBean;
import com.ganith.databinding.FragmentHomeBinding;
import com.ganith.databinding.TracksRowLayoutBinding;
import com.ganith.dialog.AddTrackDialog;
import com.ganith.util.Navigator;
import com.ganith.util.UIStaticData;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;


public class HomeFragment extends BaseFragment {

    private static final String TAG = "HomeFragment";

    private FirebaseFirestore firebaseFirestore;
    private FirestoreRecyclerOptions<TracksBean> options;

    private TrackListAdapter trackListAdapter;
    private FragmentHomeBinding fragmentHomeBinding;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseFirestore = FirebaseFirestore.getInstance();
        Query query = firebaseFirestore.collection("tracks");
        options = new FirestoreRecyclerOptions.Builder<TracksBean>()
                .setQuery(query, TracksBean.class)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        fragmentHomeBinding.swipeReFresh.setColorSchemeResources(R.color.colorGreen);
        trackListAdapter = new TrackListAdapter(options);
        fragmentHomeBinding.recyclerView.setAdapter(trackListAdapter);

        fragmentHomeBinding.swipeReFresh.setOnRefreshListener(() -> {
            trackListAdapter.updateTrackList();
            fragmentHomeBinding.swipeReFresh.setRefreshing(false);
        });

        fragmentHomeBinding.floatingActionBtn.setOnClickListener(view -> {
            AddTrackDialog addTrackDialog = new AddTrackDialog(getActivity());
            addTrackDialog.show();
        });

        return fragmentHomeBinding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        trackListAdapter.startListening();
    }

    private static class TrackListAdapter extends FirestoreRecyclerAdapter<TracksBean, TrackListAdapter.TracksBeanHolder> {

        public TrackListAdapter(@NonNull FirestoreRecyclerOptions<TracksBean> options) {
            super(options);
        }

        public void updateTrackList() {
            notifyDataSetChanged();
        }

        @Override
        protected void onBindViewHolder(@NonNull TracksBeanHolder holder, int position, @NonNull TracksBean model) {
            holder.layoutBinding.tvTrackName.setText(model.getDisplay_name());
            holder.layoutBinding.tvOwnerName.setText(model.getOwner_name() + " (" + "Owner" + ")");
            holder.layoutBinding.tvExpValue.setText("" + "(" + model.getTotal_expense() + ")" + "");
            holder.layoutBinding.tvBalanceValue.setText("" + "(" + model.getBalance() + ")" + "");
            holder.layoutBinding.tvIncomeValue.setText("" + "(" + model.getTotal_income() + ")" + "");

            holder.layoutBinding.tvTotalIncome.setOnClickListener(view -> {
                Navigator.replaceFragment(IncomeInfoFragment.newInstance());
            });

            holder.layoutBinding.tvTotalExp.setOnClickListener(view -> {
                Navigator.replaceFragment(ExpensesInfoFragment.newInstance());
            });

            holder.layoutBinding.tvAddExpenses.setOnClickListener(view -> {
                ExpensesAddFragment expensesFragment = ExpensesAddFragment.newInstance(model.getDisplay_name());
                expensesFragment.show(UIStaticData.getFragmentActivity().getSupportFragmentManager(), "EXP");
            });

            holder.layoutBinding.tvAddIncome.setOnClickListener(view -> {
                IncomeAddFragment incomeFragment = IncomeAddFragment.newInstance(model.getDisplay_name());
                incomeFragment.show(UIStaticData.getFragmentActivity().getSupportFragmentManager(), "INC");
            });

            holder.layoutBinding.tvTransfer.setOnClickListener(view -> {
                TransferFragment transferFragment = TransferFragment.newInstance(model.getDisplay_name());
                transferFragment.show(UIStaticData.getFragmentActivity().getSupportFragmentManager(), "TRA");
            });

            holder.layoutBinding.ivMore.setOnClickListener(view -> {
                Navigator.replaceFragment(TrackInfoFragment.newInstance(model));
            });
        }

        @NonNull
        @Override
        public TracksBeanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tracks_row_layout, parent, false);
            return new TracksBeanHolder(view);
        }

        private class TracksBeanHolder extends RecyclerView.ViewHolder {

            TracksRowLayoutBinding layoutBinding;

            public TracksBeanHolder(View v) {
                super(v);
                layoutBinding = TracksRowLayoutBinding.bind(v);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        trackListAdapter.stopListening();
    }
}
