package com.ganith.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ganith.R;
import com.ganith.bean.TracksBean;
import com.ganith.widget.PrefixEditText;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


public class IncomeAddFragment extends BottomSheetDialogFragment {

    private static final String TAG = "AddBuildingFragment";

    private TextInputEditText flatNoInputEditText;
    private TextInputEditText aptNameInputEditText;
    private TextInputEditText ownerNameInputEditText;
    private PrefixEditText ownerMobileInputEditText;
    private TextInputEditText amountInputEditText;
    private TextInputEditText interestedInputEditText;
    private MaterialButton addBtn;
    private CircularProgressIndicator loadingProgressBar;

    private String plotNo;
    private String reason;

    private FirebaseFirestore firestormDb;
    private TracksBean buildingsBean;

    private static final String ARG_PARAM = "trackName";
    private String trackName;


    public static IncomeAddFragment newInstance(String trackName) {
        IncomeAddFragment fragment = new IncomeAddFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, trackName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trackName = getArguments().getString(ARG_PARAM);
        }
        firestormDb = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_income, container, false);
        /*flatNoInputEditText = view.findViewById(R.id.flat_no_input_edt);
        aptNameInputEditText = view.findViewById(R.id.apt_input_edt);
        ownerNameInputEditText = view.findViewById(R.id.owner_input_edt);
        ownerMobileInputEditText = view.findViewById(R.id.et_mobile);
        amountInputEditText = view.findViewById(R.id.amount_input_edt);
        interestedInputEditText = view.findViewById(R.id.intrested_input_edt);*/
        addBtn = view.findViewById(R.id.bt_add);
        //addBtn.setOnClickListener(this);

        TextView tvTrackName = view.findViewById(R.id.tv_track_name);
        tvTrackName.setText(trackName);
        loadingProgressBar = view.findViewById(R.id.progress_loading);
        return view;
    }

    /*@Override
    public void handleOnClick(View view) {
        if (view.getId() == R.id.bt_add) {
            addBtn.setVisibility(View.INVISIBLE);
            loadingProgressBar.setVisibility(View.VISIBLE);
            addUserDetails();
        }
    }*/

    private void addUserDetails() {
        plotNo = flatNoInputEditText.getText().toString().trim();
        String apartmentName = aptNameInputEditText.getText().toString().trim();
        String ownerName = ownerNameInputEditText.getText().toString().trim();
        String ownerPhNo = ownerMobileInputEditText.getText().toString().trim();
        String amount = amountInputEditText.getText().toString().trim();
        String interested = interestedInputEditText.getText().toString().trim();
        //buildingsBean = new TracksBean(plotNo, apartmentName, ownerName, ownerPhNo, amount, interested, "");
        addDataToFireBase();
    }

    boolean isEmpty(TextInputEditText editText) {
        CharSequence str = editText.getText().toString().trim();
        return TextUtils.isEmpty(str);
    }

    private void addDataToFireBase() {
        DocumentReference docRef = firestormDb.collection("buildings").document(plotNo);
        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                addUserDocument();
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                } else {
                    Log.d(TAG, "No such document");
                    addUserDocument();
                }
            } else {
                Log.d(TAG, "get failed with ", task.getException());
            }
        });
    }

    private void addUserDocument() {
        CollectionReference users = firestormDb.collection("buildings");
        users.document(plotNo).set(buildingsBean);
        users.add(buildingsBean).addOnSuccessListener(documentReference -> Toast.makeText(getActivity(), "User Added Successfully", Toast.LENGTH_LONG).show()).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "Fail to add User \n" + e, Toast.LENGTH_LONG).show();
            }
        });
    }
}