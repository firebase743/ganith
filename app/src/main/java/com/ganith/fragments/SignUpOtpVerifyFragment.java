package com.ganith.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ganith.R;
import com.ganith.bean.UserBean;
import com.ganith.util.Navigator;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;


public class SignUpOtpVerifyFragment extends BaseFragment {

    private static final String TAG = "SignUpFragment";

    private TextInputEditText nameInputEditText;
    private TextInputEditText emailInputEditText;
    private MaterialButton signUpBtn;
    private ProgressBar loadingProgressBar;
    private EditText etBox1;
    private EditText etBox2;
    private EditText etBox3;
    private EditText etBox4;
    private EditText etBox5;
    private EditText etBox6;

    private static final String ARG_PARAM = "verificationID";
    private String mParam;

    private FirebaseFirestore firestormDb;
    private UserBean userBean;

    public static SignUpOtpVerifyFragment newInstance(String param) {
        SignUpOtpVerifyFragment fragment = new SignUpOtpVerifyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);
        }
        firestormDb = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        nameInputEditText = view.findViewById(R.id.user_name_input_edt);
        emailInputEditText = view.findViewById(R.id.email_input_edt);
        signUpBtn = view.findViewById(R.id.bt_sign_up);
        signUpBtn.setOnClickListener(this);

        etBox1 = view.findViewById(R.id.et_box1);
        etBox2 = view.findViewById(R.id.et_box2);
        etBox3 = view.findViewById(R.id.et_box3);
        etBox4 = view.findViewById(R.id.et_box4);
        etBox5 = view.findViewById(R.id.et_box5);
        etBox6 = view.findViewById(R.id.et_box6);

        loadingProgressBar = view.findViewById(R.id.progress_loading);
        editTextInput();
        return view;
    }

    private void editTextInput() {
        etBox1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox2.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox3.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox4.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox5.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox6.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void handleOnClick(View view) {
        if (view.getId() == R.id.bt_sign_up) {
            signUpBtn.setVisibility(View.INVISIBLE);
            loadingProgressBar.setVisibility(View.VISIBLE);
            if (isEmpty(etBox1) || isEmpty(etBox2) || isEmpty(etBox3) || isEmpty(etBox4) || isEmpty(etBox5) || isEmpty(etBox6)) {
                Toast.makeText(getActivity(), "OTP is not valid", Toast.LENGTH_SHORT).show();
                return;
            }
            if (mParam != null) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                signUpBtn.setVisibility(View.GONE);

                String code = etBox1.getText().toString().trim() +
                        etBox2.getText().toString().trim() +
                        etBox3.getText().toString().trim() +
                        etBox4.getText().toString().trim() +
                        etBox5.getText().toString().trim() +
                        etBox6.getText().toString().trim();

                PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(mParam, code);
                FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential).addOnCompleteListener(task -> {
                    Log.d(TAG, "onCreate: task: " + task.isSuccessful());
                    if (task.isSuccessful()) {
                        loadingProgressBar.setVisibility(View.VISIBLE);
                        signUpBtn.setVisibility(View.GONE);
                        Navigator.replaceFragment(HomeFragment.newInstance());
                    } else {
                        loadingProgressBar.setVisibility(View.GONE);
                        signUpBtn.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), "OTP is not valid", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            addUserDetails();
        }
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString().trim();
        return TextUtils.isEmpty(str);
    }

    private void addUserDetails() {
        String userName = nameInputEditText.getText().toString().trim();
        String email = emailInputEditText.getText().toString().trim();
        userBean = new UserBean(userName, email, "", "user");
        addUserDocument();
    }

    boolean isEmpty(TextInputEditText editText) {
        CharSequence str = editText.getText().toString().trim();
        return TextUtils.isEmpty(str);
    }

    /*private void addDataToFireBase() {
        DocumentReference docRef = firestormDb.collection("users").document(String.valueOf(getId()));
        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                } else {
                    Log.d(TAG, "No such document");
                    addUserDocument();
                }
            } else {
                Log.d(TAG, "get failed with ", task.getException());
            }
        });
    }*/

    private void addUserDocument() {
        CollectionReference users = firestormDb.collection("users");
        users.add(userBean).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(getActivity(), "User Registered Successfully", Toast.LENGTH_SHORT).show();
                Navigator.replaceFragment(OtpSendFragment.newInstance());
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getActivity(), "Fail to add User \n" + e, Toast.LENGTH_LONG).show();
            }
        });

    }
}