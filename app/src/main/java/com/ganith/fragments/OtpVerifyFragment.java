package com.ganith.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ganith.R;
import com.ganith.util.Navigator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public class OtpVerifyFragment extends BaseFragment {

    private static final String TAG = "OtpVerifyFragment";

    private Button verifyButton;
    private TextView tvResendOtp;
    private EditText etBox1;
    private EditText etBox2;
    private EditText etBox3;
    private EditText etBox4;
    private EditText etBox5;
    private EditText etBox6;

    private static final String ARG_PARAM = "phoneNumber";

    private String verificationId;
    private String number;
    private ProgressBar progressBar;

    public static OtpVerifyFragment newInstance(String param) {
        OtpVerifyFragment fragment = new OtpVerifyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            number = getArguments().getString(ARG_PARAM);
            Log.d(TAG, "onCreate: number ........................ " + number);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_verify, container, false);
        etBox1 = view.findViewById(R.id.et_box1);
        etBox2 = view.findViewById(R.id.et_box2);
        etBox3 = view.findViewById(R.id.et_box3);
        etBox4 = view.findViewById(R.id.et_box4);
        etBox5 = view.findViewById(R.id.et_box5);
        etBox6 = view.findViewById(R.id.et_box6);

        verifyButton = view.findViewById(R.id.bt_verify_otp);
        verifyButton.setOnClickListener(this);
        tvResendOtp = view.findViewById(R.id.tv_ResendOtp);
        progressBar = view.findViewById(R.id.pb_loading);

        TextView tvResend = view.findViewById(R.id.tv_ResendOtp);
        tvResend.setOnClickListener(this);

        editTextInput();
        return view;
    }

    private void editTextInput() {
        etBox1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox2.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox3.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox4.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox5.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etBox5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBox6.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString().trim();
        return TextUtils.isEmpty(str);
    }

    @Override
    public void handleOnClick(View view) {
        switch (view.getId()) {
            case R.id.bt_verify_otp:
                if (isEmpty(etBox1) || isEmpty(etBox2) || isEmpty(etBox3) || isEmpty(etBox4) || isEmpty(etBox5) || isEmpty(etBox6)) {
                    Toast.makeText(getActivity(), "OTP is not valid", Toast.LENGTH_SHORT).show();
                    return;
                }
                /*if (mParam != null) {
                    progressBar.setVisibility(View.VISIBLE);
                    verifyButton.setVisibility(View.GONE);

                    String code = etBox1.getText().toString().trim() +
                            etBox2.getText().toString().trim() +
                            etBox3.getText().toString().trim() +
                            etBox4.getText().toString().trim() +
                            etBox5.getText().toString().trim() +
                            etBox6.getText().toString().trim();

                    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(mParam, code);
                    FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential).addOnCompleteListener(task -> {
                        Log.d(TAG, "onCreate: task: " + task.isSuccessful());
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.VISIBLE);
                            verifyButton.setVisibility(View.GONE);
                            Navigator.replaceFragment(HomeFragment.newInstance());
                        } else {
                            progressBar.setVisibility(View.GONE);
                            verifyButton.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "OTP is not valid", Toast.LENGTH_SHORT).show();
                        }
                    });
                }*/
                break;
            /*case R.id.tv_ResendOtp:
                Navigator.replaceFragment(OtpSendFragment.newInstance());
                break;*/
            default:
                break;
        }
    }
}