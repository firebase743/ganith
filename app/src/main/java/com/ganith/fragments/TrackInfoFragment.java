package com.ganith.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.ganith.bean.TracksBean;
import com.ganith.databinding.FragmentTracksInfoBinding;
import com.google.firebase.firestore.FirebaseFirestore;

public class TrackInfoFragment extends BaseFragment {

    private static final String TAG = "TrackInfoFragment";
    private FirebaseFirestore firebaseFirestore;
    private FirestoreRecyclerOptions<TracksBean> options;

    private FragmentTracksInfoBinding layoutBinding;
    private TracksBean tracksBean;

    private static final String ARG_PARAM = "trackInfoBean";

    public static TrackInfoFragment newInstance(TracksBean tracksBean) {
        TrackInfoFragment fragment = new TrackInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, tracksBean);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseFirestore = FirebaseFirestore.getInstance();
        if (getArguments() != null) {
            tracksBean = (TracksBean) getArguments().getSerializable(ARG_PARAM);
            Log.d(TAG, "onCreate: name :>> " + tracksBean.getDisplay_name());
            Log.d(TAG, "onCreate: income : " + tracksBean.getIncome_sources());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutBinding = FragmentTracksInfoBinding.inflate(inflater, container, false);
        return layoutBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layoutBinding.tvTrackName.setText(tracksBean.getDisplay_name());
        layoutBinding.tvOwnerName.setText(tracksBean.getOwner_name());
        layoutBinding.tvBalanceValue.setText(""+tracksBean.getBalance());
        layoutBinding.tvIncomeValue.setText(""+tracksBean.getTotal_income());
        layoutBinding.tvExpValue.setText(""+tracksBean.getTotal_expense());
    }
}
