package com.ganith.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ganith.R;
import com.ganith.bean.UserBean;
import com.ganith.util.Navigator;
import com.ganith.widget.PrefixEditText;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.concurrent.TimeUnit;

public class OtpSendFragment extends BaseFragment {

    private static final String TAG = "OtpSendFragment";

    private PrefixEditText numberInputEditText;
    private MaterialButton otpSendBtn;
    private CircularProgressIndicator loadingProgressBar;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private FirebaseFirestore firestormDb;
    private FirebaseAuth firebaseAuth;
    private String verificationID;
    private boolean regNumber = false;
    String phoneNumber;

    public static OtpSendFragment newInstance() {
        OtpSendFragment fragment = new OtpSendFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firestormDb = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_send, container, false);
        numberInputEditText = view.findViewById(R.id.et_otp);
        otpSendBtn = view.findViewById(R.id.bt_sign_in);
        otpSendBtn.setOnClickListener(this);
        loadingProgressBar = view.findViewById(R.id.progress_loading);
        return view;
    }

    @Override
    public void handleOnClick(View view) {
        switch (view.getId()) {
            case R.id.bt_sign_in:
                /*phoneNumber = numberInputEditText.getText().toString().trim();
                if (phoneNumber.isEmpty() || phoneNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Invalid number", Toast.LENGTH_SHORT).show();
                    return;
                }*/
                //verifyPhNoInDB(phoneNumber);
                Navigator.replaceFragment(HomeFragment.newInstance());
                //otpSend();
                break;
            default:
                break;
        }
    }

    /*private void verifyPhNoInDB(String phNo) {
        firestormDb.collection("users").addSnapshotListener((value, error) -> {
            if (value != null) {
                for (DocumentSnapshot doc : value) {
                    String regMobile = doc.get("mobile").toString();
                    if (phNo.equals(regMobile)) {
                        //Navigator.replaceFragment(OtpVerifyFragment.newInstance(phNo));
                        regNumber = true;
                    } else {
                        //Navigator.replaceFragment(SignUpOtpVerifyFragment.newInstance(phNo));
                        regNumber = false;
                    }
                    otpSend(regNumber);
                }
            }
        });
    }*/

    /*private void otpSend() {
        Log.d(TAG, "otpSend: called.");
        loadingProgressBar.setVisibility(View.VISIBLE);
        otpSendBtn.setVisibility(View.INVISIBLE);
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted: called.");
                loadingProgressBar.setVisibility(View.GONE);
                otpSendBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                Log.d(TAG, "onVerificationFailed: called.");
                loadingProgressBar.setVisibility(View.GONE);
                otpSendBtn.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                Log.d(TAG, "onCodeSent: called.");
                loadingProgressBar.setVisibility(View.GONE);
                otpSendBtn.setVisibility(View.VISIBLE);
                *//*if(registered){
                    Navigator.replaceFragment(OtpVerifyFragment.newInstance(verificationId));
                }else{
                    UserBean userBean = new UserBean("", "", phoneNumber, "");
                    CollectionReference users = firestormDb.collection("users");
                    users.add(userBean).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            //Toast.makeText(getActivity(), "User Registered Successfully", Toast.LENGTH_SHORT).show();
                            Navigator.replaceFragment(SignUpOtpVerifyFragment.newInstance(verificationId));
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Fail to add User \n" + e, Toast.LENGTH_LONG).show();
                        }
                    });

                }*//*

                firestormDb.collection("users").addSnapshotListener((value, error) -> {
                    if (value != null) {
                        for (DocumentSnapshot doc : value) {
                            String regMobile = doc.get("mobile").toString();

                        }
                    }
                });
            }
        };

        PhoneAuthOptions phoneAuthOptions = PhoneAuthOp0tions.newBuilder(firebaseAuth)
                .setPhoneNumber("+91" + phoneNumber)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(getActivity())
                .setCallbacks(mCallback)
                .build();
        PhoneAuthProvider.verifyPhoneNumber(phoneAuthOptions);
    }*/
}