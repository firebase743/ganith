package com.ganith.fragments;

import android.view.View;

import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment implements View.OnClickListener {


    public void handleOnClick(View view) {

    }


    @Override
    public void onClick(View view) {
        handleOnClick(view);
    }
}
