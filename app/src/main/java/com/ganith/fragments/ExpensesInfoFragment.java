package com.ganith.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.ganith.R;
import com.ganith.bean.ExpansesBean;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ExpensesInfoFragment extends BaseFragment {

    private static final String TAG = "ExpensesInfoFragment";
    
    private FirebaseFirestore firebaseFirestore;
    private FirestoreRecyclerOptions<ExpansesBean> options;
    private ExpensesInfoAdapter expensesInfoAdapter;

    public static ExpensesInfoFragment newInstance() {
        ExpensesInfoFragment expensesInfoFragment = new ExpensesInfoFragment();
        return expensesInfoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseFirestore = FirebaseFirestore.getInstance();
        Query query = firebaseFirestore.collection("expenses");
        options = new FirestoreRecyclerOptions.Builder<ExpansesBean>()
                .setQuery(query, ExpansesBean.class)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        expensesInfoAdapter = new ExpensesInfoAdapter(options);
        recyclerView.setAdapter(expensesInfoAdapter);
        return view;
    }

    private static class ExpensesInfoAdapter extends FirestoreRecyclerAdapter<ExpansesBean, ExpensesInfoAdapter.ExpensesBeanHolder> {

        public ExpensesInfoAdapter(@NonNull FirestoreRecyclerOptions<ExpansesBean> options) {
            super(options);
        }

        @Override
        protected void onBindViewHolder(@NonNull ExpensesBeanHolder holder, int position, @NonNull ExpansesBean model) {
            Log.d(TAG, "" + model.getAmount());
            Log.d(TAG, "" + model.getCategory());
            Log.d(TAG, "" + model.getDescription());
            Log.d(TAG, "" + model.getPaidTo());
        }

        @NonNull
        @Override
        public ExpensesBeanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tracks_row_layout, parent, false);
            return new ExpensesBeanHolder(view);
        }

        private class ExpensesBeanHolder extends RecyclerView.ViewHolder {

            private TextView tvAmount;
            private TextView tvCategory;
            private TextView tvDesc;
            private TextView tvPayee_name;
            private TextView tvPayee_mobile;

            public ExpensesBeanHolder(View v) {
                super(v);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        expensesInfoAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        expensesInfoAdapter.stopListening();
    }
}
