package com.ganith.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ganith.databinding.FragmentTransferLayoutBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class TransferFragment extends BottomSheetDialogFragment {

    private static final String TAG = "TransferFragment";

    private static final String ARG_PARAM = "trackName";
    private String trackName;

    public static TransferFragment newInstance(String trackName) {
        TransferFragment fragment = new TransferFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, trackName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trackName = getArguments().getString(ARG_PARAM);
        }
    }

    private FragmentTransferLayoutBinding layoutBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutBinding = FragmentTransferLayoutBinding.inflate(inflater, container, false);
        layoutBinding.tvTrackName.setText(trackName);
        return layoutBinding.getRoot();
    }
}
