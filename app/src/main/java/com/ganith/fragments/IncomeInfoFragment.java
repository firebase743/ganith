package com.ganith.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.ganith.R;
import com.ganith.bean.IncomeBean;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class IncomeInfoFragment extends BaseFragment {

    private static final String TAG = "IncomeInfoFragment";

    private FirebaseFirestore firebaseFirestore;
    private FirestoreRecyclerOptions<IncomeBean> options;
    private IncomeInfoAdapter incomeInfoAdapter;

    public static IncomeInfoFragment newInstance() {
        IncomeInfoFragment fragment = new IncomeInfoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseFirestore = FirebaseFirestore.getInstance();
        Query query = firebaseFirestore.collection("incomes");
        options = new FirestoreRecyclerOptions.Builder<IncomeBean>()
                .setQuery(query, IncomeBean.class)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        incomeInfoAdapter = new IncomeInfoAdapter(options);
        recyclerView.setAdapter(incomeInfoAdapter);
        return view;
    }

    private static class IncomeInfoAdapter extends FirestoreRecyclerAdapter<IncomeBean, IncomeInfoAdapter.IncomeBeanHolder> {


        public IncomeInfoAdapter(@NonNull FirestoreRecyclerOptions<IncomeBean> options) {
            super(options);
        }

        @Override
        protected void onBindViewHolder(@NonNull IncomeBeanHolder holder, int position, @NonNull IncomeBean model) {
            Log.d(TAG, "" + model.getAmount());
            Log.d(TAG, "" + model.getIncomeSource());
            Log.d(TAG, "" + model.getDescription());
            Log.d(TAG, "" + model.getReceivedFrom());
            Log.d(TAG, "" + model.getHistory());
        }

        @NonNull
        @Override
        public IncomeBeanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tracks_row_layout, parent, false);
            return new IncomeBeanHolder(view);
        }

        private class IncomeBeanHolder extends RecyclerView.ViewHolder {

            private TextView tvAmount;
            private TextView tvIncomeSource;
            private TextView tvDesc;
            private TextView tvReceiver_name;
            private TextView tvReceiver_mobile;


            public IncomeBeanHolder(View v) {
                super(v);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        incomeInfoAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        incomeInfoAdapter.stopListening();
    }

}
