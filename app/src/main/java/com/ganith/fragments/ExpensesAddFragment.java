package com.ganith.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.ganith.databinding.FragmentAddExpensesBinding;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class ExpensesAddFragment extends BottomSheetDialogFragment {

    private static final String ARG_PARAM = "trackName";
    private String trackName;

    String[] countries = {"India", "America", "China"};

    public static ExpensesAddFragment newInstance(String trackName) {
        ExpensesAddFragment fragment = new ExpensesAddFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, trackName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trackName = getArguments().getString(ARG_PARAM);
        }
    }

    private FragmentAddExpensesBinding layoutBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutBinding = FragmentAddExpensesBinding.inflate(inflater, container, false);
        layoutBinding.tvTrackName.setText(trackName);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, countries);
        layoutBinding.categoryDropdown.setAdapter(adapter);
        layoutBinding.btAdd.setOnClickListener(view1 -> {
            Toast.makeText(getActivity(), "Category drop down text : " + layoutBinding.btAdd.getText().toString(), Toast.LENGTH_LONG).show();
        });

        return layoutBinding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener((DialogInterface.OnShowListener) dialogInterface -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialog;
            FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) bottomSheet.getParent();
            BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setPeekHeight(bottomSheet.getHeight());
            coordinatorLayout.getParent().requestLayout();
        });
        return dialog;
    }
}