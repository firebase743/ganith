package com.ganith;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

public class SplashScreenActivity extends BaseActivity {

    private Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_splash_layout);
        TextView textView = findViewById(R.id.tv_title);
        Typeface typeface = Typeface.createFromAsset(getAssets(),  "fonts/poppins_bold.ttf");
        textView.setTypeface(typeface);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim);
        textView.setAnimation(animation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(loadingRunnable, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(loadingRunnable);
    }

    private Runnable loadingRunnable = () -> {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        //finish();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    };
}
