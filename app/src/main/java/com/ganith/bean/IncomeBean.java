package com.ganith.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class IncomeBean implements Serializable {

    private String description;
    private int amount;
    private String incomeSource;
    private ArrayList<String> history;
    private HashMap<String, String> receivedBy;
    private HashMap<String, String> receivedFrom;
    private boolean valid;
    private String createdTimeStamp;
    private String updatedTimeStamp;


    public IncomeBean(String description, int amount, String incomeSource, ArrayList<String> history, HashMap<String, String> receivedBy, HashMap<String, String> receivedFrom, boolean valid, String createdTimeStamp, String updatedTimeStamp) {
        this.description = description;
        this.amount = amount;
        this.incomeSource = incomeSource;
        this.history = history;
        this.receivedBy = receivedBy;
        this.receivedFrom = receivedFrom;
        this.valid = valid;
        this.createdTimeStamp = createdTimeStamp;
        this.updatedTimeStamp = updatedTimeStamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getIncomeSource() {
        return incomeSource;
    }

    public void setIncomeSource(String incomeSource) {
        this.incomeSource = incomeSource;
    }

    public ArrayList<String> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<String> history) {
        this.history = history;
    }

    public HashMap<String, String> getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(HashMap<String, String> receivedBy) {
        this.receivedBy = receivedBy;
    }

    public HashMap<String, String> getReceivedFrom() {
        return receivedFrom;
    }

    public void setReceivedFrom(HashMap<String, String> receivedFrom) {
        this.receivedFrom = receivedFrom;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    public void setCreatedTimeStamp(String createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }

    public String getUpdatedTimeStamp() {
        return updatedTimeStamp;
    }

    public void setUpdatedTimeStamp(String updatedTimeStamp) {
        this.updatedTimeStamp = updatedTimeStamp;
    }
}
