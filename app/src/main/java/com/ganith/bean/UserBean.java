package com.ganith.bean;

import java.io.Serializable;

public class UserBean implements Serializable {

    private String userName;
    private String email;
    private String mobile;
    private String role;

    public UserBean() {
    }

    public UserBean(String userName, String email, String mobile, String role) {
        this.userName = userName;
        this.email = email;
        this.mobile = mobile;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
