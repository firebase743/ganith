package com.ganith.bean;

import java.io.Serializable;

public class TrackAccessBean implements Serializable {

    private String mobile;
    private String name;
    private boolean owner;

    public TrackAccessBean(String mobile, String name, boolean owner) {
        this.mobile = mobile;
        this.name = name;
        this.owner = owner;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }


}
