package com.ganith.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ExpansesBean implements Serializable {

    private String description;
    private int amount;
    private String category;
    private ArrayList<String> history;
    private HashMap<String, String> paidBy;
    private HashMap<String, String> paidTo;
    private boolean valid;
    private String createdTimeStamp;
    private String updatedTimeStamp;

    public ExpansesBean(String description, int amount, String category, ArrayList<String> history, HashMap<String, String> paidBy, HashMap<String, String> paidTo, boolean valid, String createdTimeStamp, String updatedTimeStamp) {
        this.description = description;
        this.amount = amount;
        this.category = category;
        this.history = history;
        this.paidBy = paidBy;
        this.paidTo = paidTo;
        this.valid = valid;
        this.createdTimeStamp = createdTimeStamp;
        this.updatedTimeStamp = updatedTimeStamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<String> getHistory() {
        return history;
    }

    public void setHistory(ArrayList history) {
        this.history = history;
    }

    public HashMap<String, String> getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(HashMap<String, String> paidBy) {
        this.paidBy = paidBy;
    }

    public HashMap<String, String> getPaidTo() {
        return paidTo;
    }

    public void setPaidTo(HashMap<String, String> paidTo) {
        this.paidTo = paidTo;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    public void setCreatedTimeStamp(String createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }

    public String getUpdatedTimeStamp() {
        return updatedTimeStamp;
    }

    public void setUpdatedTimeStamp(String updatedTimeStamp) {
        this.updatedTimeStamp = updatedTimeStamp;
    }
}
