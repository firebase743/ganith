package com.ganith.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

public class TracksBean implements Serializable {

    private int balance;
    private String display_name;
    private ArrayList<Map<String,Object>> accessible_to;
    private ArrayList<String> expense_categories;
    private ArrayList<String> income_sources;
    private String name;
    private String owner_mobile;
    private String owner_name;
    private int total_expense;
    private int total_income;

    public TracksBean(){

    }

    public TracksBean(int balance, String display_name, ArrayList<Map<String, Object>> accessible_to, ArrayList<String> expense_categories, ArrayList<String> income_sources, String name, String owner_mobile, String owner_name, int total_expense, int total_income) {
        this.balance = balance;
        this.display_name = display_name;
        this.accessible_to = accessible_to;
        this.expense_categories = expense_categories;
        this.income_sources = income_sources;
        this.name = name;
        this.owner_mobile = owner_mobile;
        this.owner_name = owner_name;
        this.total_expense = total_expense;
        this.total_income = total_income;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public ArrayList<Map<String, Object>> getAccessible_to() {
        return accessible_to;
    }

    public void setAccessible_to(ArrayList<Map<String, Object>> accessible_to) {
        this.accessible_to = accessible_to;
    }

    public ArrayList<String> getExpense_categories() {
        return expense_categories;
    }

    public void setExpense_categories(ArrayList<String> expense_categories) {
        this.expense_categories = expense_categories;
    }

    public ArrayList<String> getIncome_sources() {
        return income_sources;
    }

    public void setIncome_sources(ArrayList<String> income_sources) {
        this.income_sources = income_sources;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_mobile() {
        return owner_mobile;
    }

    public void setOwner_mobile(String owner_mobile) {
        this.owner_mobile = owner_mobile;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public int getTotal_expense() {
        return total_expense;
    }

    public void setTotal_expense(int total_expense) {
        this.total_expense = total_expense;
    }

    public int getTotal_income() {
        return total_income;
    }

    public void setTotal_income(int total_income) {
        this.total_income = total_income;
    }
}