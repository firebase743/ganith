package com.ganith.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ganith.R;
import com.ganith.bean.TracksBean;
import com.ganith.databinding.AddTrackLayoutBinding;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddTrackDialog extends Dialog {

    private static final String TAG = "AddTrackDialog";

    private AddTrackLayoutBinding layoutBinding;
    private Context context;

    public AddTrackDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        layoutBinding = AddTrackLayoutBinding.inflate(getLayoutInflater(), null, false);
        setContentView(layoutBinding.getRoot());
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.74);
        getWindow().setLayout(width, height);
        getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layoutBinding.ivClose.setOnClickListener(view -> {
            this.dismiss();
        });

        layoutBinding.btAdd.setOnClickListener(view -> {
            //Toast.makeText(context, "Track added successfully", Toast.LENGTH_SHORT).show();
            String name = layoutBinding.nameInputEdt.getText().toString().trim();
            String mobile = layoutBinding.mobileInputEdt.getText().toString().trim();
            String trackName = layoutBinding.trackNameInputEdt.getText().toString().trim();
            String expCat = layoutBinding.expCatInputEdt.getText().toString().trim();
            String incCat = layoutBinding.incCatInputEdt.getText().toString().trim();
            boolean readAccess = Boolean.parseBoolean(layoutBinding.rbAccess1.getText().toString());
            boolean writeAccess = Boolean.parseBoolean(layoutBinding.rbAccess1.getText().toString());
            Log.d(TAG, "onCreate: name " + name +" mobile : "+ mobile +" trackName: "
                    + trackName +" expCat : "+ expCat +" incCat: "+ incCat +" readAccess : "+ readAccess +" writeAccess: "+ writeAccess);

            ArrayList<Map<String, Object>> accessibleList = new ArrayList<>();
            Map accessibleMap = new HashMap();
            accessibleMap.put("mobile", mobile);
            accessibleMap.put("read", true);
            accessibleMap.put("write", true);
            accessibleList.add(accessibleMap);

            ArrayList<String> incomeList = new ArrayList();
            incomeList.add(incCat);

            ArrayList<String> expList = new ArrayList();
            expList.add(expCat);

            String trName_mobile =  mobile+"_"+trackName;
            TracksBean tracksBean = new TracksBean(0, trackName, accessibleList, expList, incomeList, trName_mobile, mobile, name, 0, 0);

            FirebaseFirestore firestore = FirebaseFirestore.getInstance();



            CollectionReference tracksRef = firestore.collection("tracks");
            DocumentReference docRef = tracksRef.document(trName_mobile);
            docRef.collection("incomes");
            docRef.collection("expenses");
            docRef.set(tracksBean).addOnSuccessListener(documentReference ->
                    Toast.makeText(context, "Track Added Successfully", Toast.LENGTH_LONG).show()).addOnFailureListener(e -> Toast.makeText(context, "Fail to add Track \n" + e, Toast.LENGTH_LONG).show());

            this.dismiss();
        });
    }
}
